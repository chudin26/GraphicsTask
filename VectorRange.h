#ifndef VECTORRANGE_H
#define VECTORRANGE_H

#include <QVector>
#include <functional>

template <class T> class VectorRange
{
public:
	typedef QVector <T> Vector;
	typedef typename QVector <T>::Iterator Iterator;

public:
	VectorRange(const Iterator &begin, const Iterator &end)
		: _begin(begin)
		, _end(end)
	{
	}

	VectorRange(QVector <T> &vector, int position, int length = -1)
		: _begin(vector.begin() + position)
		, _end(vector.begin() + position + (length == -1 ? (vector.size() - position) : length))
	{
	}

	VectorRange(QVector <T> &vector)
		: _begin(vector.begin())
		, _end(vector.end())
	{
	}

	const Iterator& begin() const { return _begin; }
	const Iterator& end() const { return _end; }

	int length() const {
		return _end - _begin;
	}

	void forEach(std::function <void (T&)> func)
	{
		for (Iterator it = _begin; it != _end; it++) {
			func(*it);
		}
	}

//	static operator VectorRange(QVector <T> &vector) {
//		return VectorRange <T> (vector);
//	}

private:
	Iterator _begin;
	Iterator _end;
};

#endif // VECTORRANGE_H
