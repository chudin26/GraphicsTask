#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);

private slots:
	void increaseEyes();
	void decreaseEyes();
	void rotateLeftEyeAround();
	void rotateRightEye();
	void reset();

private:
	void update();
	void scaleEyes(float scale);

	QSharedPointer <Ui::MainWindow> ui;

	QSharedPointer <class Face> face;
};

#endif // MAINWINDOW_H
