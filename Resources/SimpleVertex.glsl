attribute vec4 Position;

uniform mat4 View;
uniform vec4 Color;
uniform float PointSize;

varying vec4 DestinationColor;

void main(void) {
	DestinationColor = Color;
	gl_Position = View * Position;
	gl_PointSize = PointSize;
}
