#include "Face.h"

const Face::Element Face::LEFT_EYE = Face::Element { 42, 6 };
const Face::Element Face::RIGHT_EYE = Face::Element { 36, 6 };
const Face::Element Face::MOUTH = Face::Element { 48, 20 };
const Face::Element Face::NOSE = Face::Element { 27, 9 };
const Face::Element Face::JAW = Face::Element { 0, 17 };

Face::Face(const QString &csvFileName)
	: csvFileName(csvFileName)
{
	reset();
}

VectorRange <math::Point> Face::getPoints(const Element &element)
{
	return VectorRange <math::Point> (points, element.pos, element.length);
}

void Face::reset()
{
	points = math::Utils::loadFromFile(csvFileName);
	center = math::Utils::getCenterOfPoints(points);
}
