#include "GLWidget.h"
#include <QOpenGLShaderProgram>
#include <QOpenGLTexture>
#include <QMouseEvent>
#include <QApplication>
#include <QScreen>

GLWidget::GLWidget(QWidget *parent)
	: QOpenGLWidget(parent),
	  clearColor(Qt::black)
{
	setPointSize(5.0f);
}

GLWidget::~GLWidget()
{
}

QByteArray GLWidget::loadShaderFromFile(const QString &fileName)
{
	QFile shaderFile(fileName);
	shaderFile.open(QIODevice::ReadOnly);

	auto result = shaderFile.readAll();
	shaderFile.close();

	return result;
}

void GLWidget::initializeGL()
{
	initializeOpenGLFunctions();

	glEnable(GL_VERTEX_PROGRAM_POINT_SIZE);

	QOpenGLShader *vShader = new QOpenGLShader(QOpenGLShader::Vertex, this);
	auto vShaderSrc = loadShaderFromFile(":/shaders/SimpleVertex.glsl");
	vShader->compileSourceCode(vShaderSrc.begin());

	QOpenGLShader *fShader = new QOpenGLShader(QOpenGLShader::Fragment, this);
	auto fShaderSrc = loadShaderFromFile(":/shaders/SimpleFragment.glsl");
	fShader->compileSourceCode(fShaderSrc.begin());

	program = new QOpenGLShaderProgram;
	program->addShader(vShader);
	program->addShader(fShader);
	program->link();

	vertexAttribute = program->attributeLocation("Position");

	program->bind();
}

void GLWidget::paintGL()
{
	glClearColor(clearColor.redF(), clearColor.greenF(), clearColor.blueF(), clearColor.alphaF());
	glClear(GL_COLOR_BUFFER_BIT);

	program->setUniformValue("Color", solidColor);
	program->setUniformValue("PointSize", pointSize);
	program->enableAttributeArray(0);
	program->setAttributeArray(vertexAttribute, vertData.begin(), 3, 3 * sizeof(float));

	QMatrix4x4 m;
	m.scale(1.5f, -1.5f);
	m.translate(-0.5f, -0.5f, 0.0f);
	program->setUniformValue("View", m);

	program->bind();

	if (vertData.size()) {
		glDrawArrays(GL_POINTS, 0, vertData.size() / 3);
	}
}

void GLWidget::resizeGL(int width, int height)
{
	int side = qMin(width, height);
	glViewport((width - side) / 2, (height - side) / 2, side, side);
}

void GLWidget::setPoints(const QVector <QVector2D> &points)
{
	vertData.clear();
	vertData.reserve(points.size() * 3);

	for (auto point: points) {
		vertData.append(point.x());
		vertData.append(point.y());
		vertData.append(0.0f);
	}

	update();
}

void GLWidget::setClearColor(const QColor &color)
{
	clearColor = color;
}

void GLWidget::setSolidColor(const QColor &color)
{
	solidColor = color;
}

void GLWidget::setPointSize(float size)
{
	pointSize = QApplication::primaryScreen()->devicePixelRatio() * size;
}
