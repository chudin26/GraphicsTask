#ifndef UTILS_H
#define UTILS_H

#include <QVector>
#include <QVector2D>
#include <QString>
#include <VectorRange.h>

namespace math {

typedef QVector2D Point;
typedef QVector <Point> Points;

class Utils
{
public:
	static Points loadFromData(const QByteArray &data);
	static Points loadFromFile(const QString &fileName);

	static void scalePoints(VectorRange <Point> &points, float scale);
	static void scalePoints(VectorRange <Point> &points, float scale, const Point &anchorPoint);

	static void rotatePoints(VectorRange <Point> &points, float angle, const Point &anchorPoint);  // angle in degrees

	static Point getCenterOfPoints(const VectorRange <Point> &points);
};

}

#endif // UTILS_H
