#include "MainWindow.h"
#include "ui_mainwindow.h"

#include "Utils.h"
#include "Face.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
	ui(new Ui::MainWindow),
	face(new Face(":/data/landmarks.csv"))
{
	ui->setupUi(this);

	ui->glWidget->setClearColor(Qt::lightGray);
	ui->glWidget->setSolidColor(Qt::black);
	update();
}

void MainWindow::decreaseEyes()
{
	scaleEyes(0.8f);
}

void MainWindow::increaseEyes()
{
	scaleEyes(1.25f);
}

void MainWindow::reset()
{
	face->reset();
	update();
}

void MainWindow::rotateLeftEyeAround()
{
	auto points = face->getPoints(Face::LEFT_EYE);
	math::Utils::rotatePoints(points, 15.0f, face->getCenter());
	update();
}

void MainWindow::rotateRightEye()
{
	auto points = face->getPoints(Face::RIGHT_EYE);
	auto center = math::Utils::getCenterOfPoints(points);
	math::Utils::rotatePoints(points, -15.0f, center);
	update();
}

void MainWindow::scaleEyes(float scale)
{
	std::vector <Face::Element> eyes = { Face::LEFT_EYE, Face::RIGHT_EYE };
	for (const auto &element: eyes) {
		auto points = face->getPoints(element);
		math::Utils::scalePoints(points, scale);
	}

	update();
}

void MainWindow::update()
{
	ui->glWidget->setPoints(face->getPoints());
	ui->glWidget->update();
}
