#include "Utils.h"
#include <QtMath>
#include <QFile>

namespace math {

Points Utils::loadFromData(const QByteArray &data)
{
	Points points;

	const auto numbers = data.split(';');
	for (auto it = numbers.begin(); it != numbers.end(); ) {
		auto x = (*it++).toFloat();
		auto y = (*it++).toFloat();
		points.append(Point(x, y));
	}

	return points;
}

Points Utils::loadFromFile(const QString &fileName)
{
	QFile file(fileName);
	file.open(QIODevice::ReadOnly);

	auto bytes = file.readAll();
	file.close();

	return loadFromData(bytes);
}

void Utils::scalePoints(VectorRange <Point> &points, float scale)
{
	auto center = getCenterOfPoints(points);
	scalePoints(points, scale, center);
}

void Utils::scalePoints(VectorRange <Point> &points, float scale, const Point &anchorPoint)
{
	for (auto &point: points) {
		point = anchorPoint + (point - anchorPoint) * scale;
	}
}

void Utils::rotatePoints(VectorRange <Point> &points, float angle, const Point &anchorPoint)
{
	float a = M_PI / 180 * angle;
	float cs = cosf(a);
	float sn = sinf(a);

	for (auto &point: points) {
		auto vector = point - anchorPoint;

		auto px = vector.x() * cs - vector.y() * sn;
		auto py = vector.x() * sn + vector.y() * cs;
		point = anchorPoint + Point(px, py);
	}
}

Point Utils::getCenterOfPoints(const VectorRange <Point> &points)
{
	Point center(0.0f, 0.0f);
	for (const auto &point: points) {
		center += point;
	}

	return center / points.length();
}

}
