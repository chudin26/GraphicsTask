#ifndef GLWIDGET_H
#define GLWIDGET_H

#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QOpenGLBuffer>

QT_FORWARD_DECLARE_CLASS(QOpenGLShaderProgram)

class GLWidget : public QOpenGLWidget, protected QOpenGLFunctions
{
	Q_OBJECT

public:
	explicit GLWidget(QWidget *parent = 0);
	~GLWidget();

	void setClearColor(const QColor &color);
	void setSolidColor(const QColor &color);
	void setPointSize(float size);

	void setPoints(const QVector <QVector2D> &points);

protected:
	void initializeGL() override;
	void paintGL() override;
	void resizeGL(int width, int height) override;

private:
	QByteArray loadShaderFromFile(const QString &fileName);

	QColor clearColor;
	QColor solidColor;
	float pointSize;

	QVector <GLfloat> vertData;

	int vertexAttribute;
	QOpenGLShaderProgram *program;
};

#endif // GLWIDGET_H
