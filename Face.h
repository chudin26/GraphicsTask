#ifndef FACE_H
#define FACE_H

#include "Utils.h"

class Face
{
public:
	struct Element {
		unsigned char pos, length;
	};

	static const Element LEFT_EYE;
	static const Element RIGHT_EYE;
	static const Element MOUTH;
	static const Element NOSE;
	static const Element JAW;

public:
	Face(const QString &csvFileName);

	VectorRange <math::Point> getPoints(const Element &element);
	math::Points getPoints() const { return points; }
	math::Point getCenter() const { return center; }

	void reset();

private:
	math::Points points;
	math::Point center;

	const QString csvFileName;
};

#endif // FACE_H
